﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Nethereum.Web3;
using Nethereum.Web3.Accounts;
using Nethereum.Contracts;
using Nethereum.RPC.Eth.DTOs;

public class BlockChainTest : MonoBehaviour {
    public const string networkUrl= "https://ropsten.infura.io";
    public string address;
    public string privateKey;
    public string ABI;
    public string contractAddress;


    private Web3 web3;
    private Contract contract;

    void Start() {
        var account = new Account(privateKey);
        web3 = new Web3(account, networkUrl,null);
        contract = web3.Eth.GetContract(ABI, contractAddress);

        GetBalanceAsync();
        CallGetCountAsync();
        CallIncreaseAsync();
    }


    private async void GetBalanceAsync() {
        var balance = await web3.Eth.GetBalance.SendRequestAsync(address) ;
        var etherAmount = Web3.Convert.FromWei(balance.Value);
        Debug.Log($"이더 보유량: {etherAmount.ToString()}");
    }

    private async void CallGetCountAsync() {
        var getCountFunc = contract.GetFunction("GetCount");
        var gas = await getCountFunc.EstimateGasAsync();
        int value = await getCountFunc.CallAsync<int>();
        Debug.Log($"GetCount 실행함 {value.ToString()}");
    }
    private async void CallIncreaseAsync() {
        var increaseFunc = contract.GetFunction("Increase");
        var gas = await increaseFunc.EstimateGasAsync();
        var returnValue = await increaseFunc.SendTransactionAsync(from: address, gas: gas, value: null, functionInput: null);
        Debug.Log($"Increase 실행함 ");
    }
}

