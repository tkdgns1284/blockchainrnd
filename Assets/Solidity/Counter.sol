pragma solidity >=0.4.22 <0.6.0;
contract Counter {
    int count;
    
    constructor() public {
        count = 0;
    }
    
    function Increase() public {
        count++;
    }
    function Reset() public {
        count = 0;
    }
    
    function GetCount() public view returns(int) {
        return count;
    }
}